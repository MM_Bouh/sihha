
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import serializers
from .models import *
from rest_framework_simplejwt.tokens import RefreshToken, TokenError
from medical_record.models import MedicalRecord


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        # Add extra responses here
        user = {}
        user['id'] = self.user.id
        user['email'] = self.user.email
        user['tel'] = self.user.tel
        user['username'] = self.user.username
        user['first_name'] = self.user.first_name
        user['last_name'] = self.user.last_name
        user['role'] = self.user.role
        user['profile_img_url'] = self.user.profile_img_url
        #self.user.groups.values_list('name', flat=True)
        data['user'] = user
        return data


class UserSerializer(serializers.ModelSerializer):
    specialityName = serializers.ReadOnlyField(source='speciality.name')

    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'last_name', 'first_name', 'role',
                  'profile_img_url', 'tel', 'email', 'experience', 'specialityName',)


class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    default_error_message = {
        'bad_token': ('Token is expired or invalid')
    }

    def validate(self, attrs):
        self.token = attrs['refresh']
        return attrs

    def save(self, **kwargs):

        try:
            RefreshToken(self.token).blacklist()

        except TokenError:
            self.fail('bad_token')

#########################################################################################
# Modify Doctor/Patient personal Info serializer


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('email', 'tel', 'username', 'first_name',
                  'last_name', 'profile_img_url')


class CreateUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('username', 'last_name', 'first_name',
                  'role', 'profile_img_url', 'tel', 'email')

    def create_user(self, username, first_name, last_name, password, **other_fields):
        other_fields.setdefault('is_active', True)
        if not username:
            # raise ValueError(_('You must provide an username field'))
            raise ValueError(('You must provide an username field'))
        # email = self.normalize_email(email)
        user = self.model(username=username, last_name=last_name,
                          first_name=first_name, **other_fields)
        user.set_password(password)
        user.save()
        return user

#########################################################################################
# All doctors  serializer


class DoctorsSerializer(serializers.ModelSerializer):
    specialityName = serializers.ReadOnlyField(source='speciality.name')

    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'experience',
                  'specialityName', 'tel', 'profile_img_url')

#########################################################################################
# All Patients  serializer


class PatientsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('id', 'username')


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        fields = ('id', 'name', 'description', 'image')
