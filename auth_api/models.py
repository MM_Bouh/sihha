from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager, Group
from django.utils import timezone
from django.contrib.auth.hashers import check_password

##########################################################################################
#Custom Account Manager   
class CustomAccountManager(BaseUserManager):
    def create_superuser(self, tel,username, first_name, last_name, email,role,profile_img_url,password, **other_fields):
        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)
        other_fields.setdefault('is_active', True)
        if other_fields.get('is_staff') is not True:
            raise ValueError(
                'Superuser must be assigned to is_staff=True.')
        if other_fields.get('is_superuser') is not True:
            raise ValueError(
                'Superuser must be assigned to is_superuser=True.')
        return self.create_user(tel=tel,username=username, last_name=last_name,
                          first_name=first_name,email=email,role=role,profile_img_url=profile_img_url,password=password, **other_fields)
    def create_user(self, username, first_name, last_name, tel,email,role,profile_img_url,password, **other_fields):
        other_fields.setdefault('is_active', True)
        if not tel:
            raise ValueError(_('You must provide a telephone number field'))
        # email = self.normalize_email(email)
        user = self.model(tel=tel,username=username, last_name=last_name,
                          first_name=first_name,email=email,role=role,profile_img_url=profile_img_url, **other_fields)
        user.set_password(password)
        user.save()
        return user


class Category(models.Model):
    name = models.CharField(max_length=60,blank=True)
    description=models.CharField(max_length=300, blank=True, )
    image = models.FileField(blank=True,null=True, upload_to='media/icons') 
    def __str__(self):
        return self.name


##########################################################################################
#Custom User  
class CustomUser(AbstractBaseUser, PermissionsMixin):

   

    # SUPER_ADMIN = 'SUPER_ADMIN'
    # SUPERVISEUR = 'SUPERVISEUR'
    DOCTOR = 'DOCTOR'
    PATIENT = 'PATIENT'
    ADMIN_AGENT = 'ADMIN_AGENT'
    ROLES = [
        (DOCTOR, DOCTOR),
        (PATIENT, PATIENT),
        (ADMIN_AGENT, ADMIN_AGENT),
        
    ]
    username = models.CharField(max_length=150)
    first_name = models.CharField(max_length=150, blank=True)
    last_name = models.CharField(max_length=150, blank=True)
    profile_img_url = models.CharField(max_length=150, blank=True)
    start_date = models.DateTimeField(default=timezone.now)
    role = models.CharField(
        max_length=50,
        choices=ROLES,
    )
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    tel = models.CharField( max_length=17, blank=True,unique=True) 
    email = models.EmailField(null=True,blank=True)
    speciality = models.ForeignKey(Category,on_delete=models.CASCADE, null=True)
    experience = models.IntegerField(max_length=10, null=True)
    objects = CustomAccountManager()
    #A string describing the name of the field on the user model that is used as the unique identifier
    USERNAME_FIELD = 'tel' # Here needs to change because the jwt default behavior search for a username_field constant 
    REQUIRED_FIELDS = ['username','first_name', 'last_name', 'email','role','profile_img_url']
   

    def __str__(self):
        return self.username

    class Meta:
        db_table = "User"


