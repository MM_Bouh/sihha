from django.contrib import admin
from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.exceptions import ValidationError

#Imports for being able to delete the outstanding token
from rest_framework_simplejwt.token_blacklist.admin import OutstandingTokenAdmin
from rest_framework_simplejwt.token_blacklist.models import OutstandingToken


class CustomOutstandingTokenAdmin(OutstandingTokenAdmin):
    def has_delete_permission(self, *args, **kwargs):
        return True



from .models import CustomUser, Category

##########################################################################################################################
##########################################################################################################################
########################                    CUSTOM LAYOUT FOR ADDING A USER                    ###########################
##########################################################################################################################
##########################################################################################################################


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = CustomUser
        fields = '__all__'

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    disabled password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = CustomUser
        fields = '__all__'


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'role', 'is_staff','tel','email')
    list_filter = ('id',)
    fieldsets = (
        (None, {'fields': ('username', 'role')}),
        ('Personal info', {'fields': ('last_name','first_name','tel','email')}),
        ('Permissions', {'fields': ('is_staff',)}),
        ('Doctor fields', {'fields': ('speciality','experience',)}),
        ('ProfileImage', {'fields': ('profile_img_url',)}),
    )
    
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username','first_name','last_name','role', 'speciality','experience','profile_img_url','tel','email', 'password1', 'password2'),
        }),
    )
    search_fields = ('username',)
    ordering = ('id',)
    filter_horizontal = ()

# class Category(Category):
#     list_display = ('name', 'description')
#     fieldsets = (
#         (None, {'fields': ('name', 'description', 'image')}),
#         )
#     add_fieldsets = (
#         (None, {
#             'classes': ('wide',),
#             'fields': ('username','first_name','last_name','role', 'speciality','experience','profile_img_url','tel','email', 'password1', 'password2'),
#         }),
#     )

# Now register the new UserAdmin...
admin.site.register(CustomUser, UserAdmin)
admin.site.unregister(Group)
#Outstanding Token config for when we delete a user we can delete also the outstading token related to it 
admin.site.unregister(OutstandingToken)
admin.site.register(OutstandingToken, CustomOutstandingTokenAdmin)
admin.site.register(Category)