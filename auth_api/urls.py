from django.urls import path
from rest_framework_simplejwt import views as jwt_views
from .views import *

urlpatterns = [
    path('login/', MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('create_patient/', create_patient),
    path('create_doctor/', create_doctor),
    path('get_current_user/', Current_User_API.as_view(), name='current_user'),

    path('doctors/', get_all_doctors),
    path('doctorsR/', get_all_doctors_redux),
    path('specialities/', get_all_specialities),
    path('patients/', get_all_patients),
    path('logout/', BlacklistTokenUpdateView.as_view(), name='blacklist'),

    path('update_doctor_profile/<int:pk>', put_doctor_profile),
    path('update_patient_profile/<int:pk>', put_patient_profile),
]
