from rest_framework_simplejwt.views import TokenObtainPairView
from .serializers import *
from rest_framework import status, generics, viewsets
from rest_framework.permissions import AllowAny, IsAuthenticated
from .models import *
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.views import APIView
from rest_framework import response
from rest_framework.decorators import api_view
from medical_record.models import MedicalRecord, Question
from medical_record.models import Response




#################################################################################################################
#################################################################################################################
#######################################              JWT TOKENS               ###################################
#################################################################################################################
#################################################################################################################
class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


class Current_User_API(generics.RetrieveAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user


class BlacklistTokenUpdateView(APIView):
    permission_classes = [AllowAny]
    authentication_classes = ()

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()
            return response.Response(status=status.HTTP_205_RESET_CONTENT)
        except:
            return response.Response(status=status.HTTP_400_BAD_REQUEST)
#################################################################################################################
#################################################################################################################
#######################################         END  JWT TOKENS               ###################################
#################################################################################################################
#################################################################################################################


#################################################################################################################
# create a doctror
@api_view(['POST'])
def create_doctor(request):
    data = request.data
    tel = request.data['tel']
    tel_exist = CustomUser.objects.filter(tel=tel).count()
    if(tel_exist == 1):
        return response.Response({'error': 'already exist'})
    else:
        speciality = Category.objects.get(id=data['speciality'])
        user = CustomUser.objects.create_user(username=data['username'], first_name=data['first_name'], last_name=data['last_name'], email=data['email'], tel=data['tel'],
                                              role=data['role'], speciality=speciality, experience=int(data['experience']), profile_img_url=data['profile_img_url'], password=data['password'])
        user.save()
        serializer = CreateUserSerializer(user)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED)

#################################################################################################################
# create a doctor


@api_view(['POST'])
def create_patient(request):
    data = request.data
    tel = request.data['tel']
    tel_exist = CustomUser.objects.filter(tel=tel).count()
    if(tel_exist == 1):
        return response.Response({'error': 'already exist'})
    else:
        user = CustomUser.objects.create_user(username=data['username'], first_name=data['first_name'], last_name=data['last_name'],
                                              email=data['email'], tel=data['tel'], role=data['role'], profile_img_url=data['profile_img_url'], password=data['password'])
        user.save()
        medical_record = MedicalRecord(patient=user)
        medical_record.save()
        question = Question.objects.all()
        for i in range(21):
            answer = Response.objects.create(
                question=question[i], medical_record=medical_record)

        answers = Response.objects.filter(medical_record=medical_record)
        print(answers[0].medical_record)
        serializer = CreateUserSerializer(user)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED)


#################################################################################################################
# PUT doctor profile
@api_view(['PUT'])
def put_doctor_profile(request, pk):

    # 1.get the doctor
    doctor = CustomUser.objects.get(id=pk)

    # 2.Create the serializer
    serializer = UserProfileSerializer(doctor, data=request.data)

    # 3.Check Srializer validity
    if(serializer.is_valid()):
        serializer.save()
        return response.Response(serializer.data)
    return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#################################################################################################################
# PUT patient profile
@api_view(['PUT'])
def put_patient_profile(request, pk):

    # 1.get the doctor
    patient = CustomUser.objects.get(id=pk)

    # 2.Create the serializer
    serializer = UserProfileSerializer(patient, data=request.data)

    # 3.Check Srializer validity
    if(serializer.is_valid()):
        serializer.save()
        return response.Response(serializer.data)
    return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#########################################################################################
# Get all doctors


@api_view(['GET'])
def get_all_doctors(request):

    doctors = CustomUser.objects.filter(role='DOCTOR')

    serializer = DoctorsSerializer(doctors, many=True)
    #print("These are the doctors", serializer.data)
    return response.Response(serializer.data)
    # return Response({'doctors': serializer.data, 'page': page, 'pages': paginator.num_pages})

#########################################################################################


@api_view(['GET'])
def get_all_doctors_redux(request):
    doctors = CustomUser.objects.filter(role='DOCTOR')
    # if len(doctors) == 0:
    #     content = {'hh': 'Error in your request'}
    #     print(content)
    #     return response.Response(content, status=status.HTTP_400_BAD_REQUEST)
    serializer = DoctorsSerializer(doctors, many=True)
    #print("get_all_doctors_redux", serializer.data)
    return response.Response({'doctors': serializer.data})

# Get all patents


@api_view(['GET'])
def get_all_patients(request):

    patients = CustomUser.objects.filter(role='PATIENT')
    serializer = PatientsSerializer(patients, many=True)
    return response.Response(serializer.data)


# Get all specialities
@api_view(['GET'])
def get_all_specialities(request):

    # Get specialities from the database
    specialities = Category.objects.all()
    serializer = CategorySerializer(specialities, many=True)
    # return response.Response(serializer.data)
    return response.Response({'specialities': serializer.data})
