# Generated by Django 3.1.7 on 2021-04-12 11:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth_api', '0007_auto_20210412_1011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customuser',
            name='role',
            field=models.CharField(choices=[('DOCTOR', 'DOCTOR'), ('PATIENT', 'PATIENT'), ('ADMIN_AGENT', 'ADMIN_AGENT')], max_length=50),
        ),
    ]
