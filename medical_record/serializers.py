from rest_framework import serializers
from .models import *

##########################################################################################
class MedicalRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicalRecord
        fields = ('id','age','height','weight','blood_type','gender')

##########################################################################################
class ListResponsesSerializer(serializers.ListSerializer):
    def update(self, instances, validated_data):
        
        instance_hash = {index: instance for index, instance in enumerate(instances)}

        result = [
            self.child.update(instance_hash[index], attrs)
            for index, attrs in enumerate(validated_data)
        ]
        return result

##########################################################################################
class ResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Response
        fields = '__all__'
        list_serializer_class = ListResponsesSerializer 

##########################################################################################
class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'

##########################################################################################
class MedicalFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicalFile
        fields = '__all__'

