from django.urls import path
from .views import *

urlpatterns = [
    path('get_questions_questionnaire/',get_questions_quetionnaire),
    path('answer_medical_record_questionnaire/',answer_medical_record_questionnaire),
    path('answer_medical_record_basic_info/',answer_medical_record_basic_info),

    path('update_medical_record_questionnaire/<int:pk>',medical_record_questionnaire),
    path('update_medical_record_basic_info/<int:pk>',medical_record_basic_info),
    path('medical_files/<int:pk>',medical_files_view),

]
