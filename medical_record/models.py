from django.db import models
from auth_api.models import *

##########################################################################################
#MEDICAL RECORD MODEL
class MedicalRecord(models.Model):
    
    #Blood Types
    A_PLUS = 'A+'
    A_MINUS = 'A-'
    B_PLUS = 'B+'
    B_MINUS = 'B-'
    AB_PLUS = 'AB+'
    AB_MINUS = 'AB-'
    O_PLUS = 'O+'
    O_MINUS = 'O-'

    BLOOD_TYPES = [
        (A_PLUS, A_PLUS),
        (A_MINUS, A_MINUS),
        (B_PLUS, B_PLUS),
        (B_MINUS, B_MINUS),
        (AB_PLUS, AB_PLUS),
        (AB_MINUS, AB_MINUS),
        (O_PLUS, O_PLUS),
        (O_MINUS, O_MINUS),
    ]


    #Gender Types
    MALE = 'male'
    FEMALE = 'female'

    GENDER = [
        (MALE,MALE),
        (FEMALE,FEMALE),
    ]

    #Model Attributes
    age = models.IntegerField(blank=True,null=True)
    height = models.IntegerField(blank=True,null=True)
    weight = models.IntegerField(blank=True,null=True)
    blood_type = models.CharField(max_length=60,choices=BLOOD_TYPES,blank=True,default='')
    gender = models.CharField(max_length=60,choices=GENDER,blank=True)
    patient = models.ForeignKey(CustomUser,on_delete=models.CASCADE)

    def __str__(self):
        return self.patient.username
    
##########################################################################################
#Question Model
class Question(models.Model):

    #Model Attributes
    content = models.CharField(max_length=512)
    has_details = models.BooleanField(null=True)
    def __str__(self):
        return self.content

##########################################################################################
#Answer Model
class Response(models.Model):

    #Model Attributes
    id = models.AutoField(primary_key=True)
    answer = models.CharField(blank=True,null=True,max_length=512)
    choice = models.BooleanField(blank=True,null=True)
    question = models.ForeignKey(Question,on_delete=models.CASCADE)
    medical_record = models.ForeignKey(MedicalRecord,on_delete=models.CASCADE)
    def __str__(self):
        return self.question.content
    

##########################################################################################
#Medical File Model
class MedicalFile(models.Model):

    #Model Attributes
    medical_file = models.FileField(blank=True,null=True, upload_to='media') 
    medical_record = models.ForeignKey(MedicalRecord,on_delete=models.CASCADE) 


