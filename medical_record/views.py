from django.utils.translation import deactivate_all
from rest_framework import response
from rest_framework import status
from rest_framework.decorators import api_view
from .serializers import *
from .models import *

# Importing os for deleting the uploaded files in the Folder Media
import os


#################################################################################################################
# Modify or Get the Basic Info Form
@api_view(['PUT', 'GET'])
def medical_record_basic_info(request, pk):

    # Check if primary key (pk) exist
    try:
        medical_record = MedicalRecord.objects.get(patient=pk)
    except MedicalRecord.DoesNotExist:
        return response.Response(status=status.HTTP_404_NOT_FOUND)

    # To Modifiy a medical record basic info (PUT request)
    if request.method == 'PUT':
        serializer = MedicalRecordSerializer(
            medical_record, data=request.data, many=False)
        if serializer.is_valid():
            serializer.save()
            print(serializer.data)
            return response.Response(serializer.data)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # To get a medical record basic info (GET request)
    if request.method == 'GET':
        serializer = MedicalRecordSerializer(medical_record)
        return response.Response(serializer.data)

#########################################################################
# Add the basic info Form


@api_view(['POST'])
def answer_medical_record_basic_info(request):
    data = request.data
    patient = CustomUser.objects.get(id=data['patient'])
    # print(patient)
    answer = MedicalRecord.objects.create(age=int(data['age']), height=int(
        data['height']), blood_type=data['blood_type'], weight=int(data['weight']), gender=data['gender'], patient=patient)
    #answer = MedicalRecord(age=int(data['age']),height=int(data['height']),blood_type=data['blood_type'], weight=int(data['weight']), gender=data['gender'], patient=patient)
    print("OK")
    answer.save()
    print("OK2")
    serializer = MedicalRecordSerializer(answer)
    return response.Response(serializer.data, status=status.HTTP_201_CREATED)

#################################################################################################################
# Modify or the get the Questionnaire Form


@api_view(['PUT', 'GET'])
def medical_record_questionnaire(request, pk):

    # Check if primary key (pk) exist
    try:
        medical_record = MedicalRecord.objects.get(patient=pk)
        responses = Response.objects.filter(medical_record=medical_record)
    except Response.DoesNotExist:
        return response.Response(status=status.HTTP_404_NOT_FOUND)

    # To Modifiy the responses in the medical questionnaire (PUT request)
    if request.method == 'PUT':
        serializer = ResponseSerializer(
            responses, data=request.data['answers'], many=True)
        print(serializer)
        if serializer.is_valid():
            serializer.save()
            return response.Response(serializer.data)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # To get answers (GET request)
    if request.method == 'GET':
        serializer = ResponseSerializer(responses, many=True)
        return response.Response(serializer.data)


#################################################################################################################
# Modify or the get the Questionnaire Form
@api_view(['POST'])
def answer_medical_record_questionnaire(request):
    # Check if primary key (pk) exist
    data = request.data
    print('questions: **************************')
    # print(data['questions'])
    print('answers: **************************')
    # print(data['answers'])
    # print(data['patient'])
    print("Before *****************************************")
    medical_record = MedicalRecord.objects.get(patient=data['patient'])
    print(medical_record)
    print("After")
    questionIds = [question["id"] for question in data['questions']]
    print(questionIds)
    #question = Question.objects.get(id=data['questions'])
    for questionId in questionIds:
        question = Question.objects.get(id=questionId)
        responses = Response.objects.create(
            medical_record=medical_record, answer=data['answers'], question=question)
        responses.save()

    serializer = ResponseSerializer(responses)
    return response.Response(serializer.data,  status=status.HTTP_201_CREATED)
#################################################################################################################
# Get the Questions for the questionnaire


@api_view(['GET'])
def get_questions_quetionnaire(request):

    # Get questions from the database
    questions = Question.objects.all()
    serializer = QuestionSerializer(questions, many=True)
    return response.Response(serializer.data)


#################################################################################################################
# Get/Delete/Post Medical Files
@api_view(['GET', 'POST', 'DELETE'])
def medical_files_view(request, pk):

    # Check if primary key (pk) exist (here pk will be indicating the medical record )
    try:
        files = MedicalFile.objects.filter(medical_record=pk)
    except MedicalFile.DoesNotExist:
        return response.Response(status=status.HTTP_404_NOT_FOUND)

    # GET  Medical Files
    if request.method == 'GET':
        serializer = MedicalFileSerializer(files, many=True)
        print("these are the files: ", serializer.data)
        return response.Response(serializer.data)

    # POST  Medical Files
    if request.method == 'POST':
        serializer = MedicalFileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return response.Response(serializer.data, status=status.HTTP_201_CREATED)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # DELET Medical File
    if request.method == 'DELETE':
        # Because the ((pk)) comming from the frontend when we do the delete method will indicate the ((id)) of the file we want to delete
        medical_file = MedicalFile.objects.get(id=pk)
        # path of the file to delete
        path_to_delete = medical_file.medical_file.path
        # remove the row
        medical_file.delete()
        # remove the file
        os.remove(path_to_delete)
        return response.Response(status=status.HTTP_204_NO_CONTENT)
