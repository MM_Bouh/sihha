# Generated by Django 3.2.6 on 2021-09-10 07:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('medical_record', '0009_alter_response_choice'),
    ]

    operations = [
        migrations.AlterField(
            model_name='response',
            name='choice',
            field=models.BooleanField(blank=True, null=True),
        ),
    ]
