# Generated by Django 3.2.6 on 2021-09-05 23:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('medical_record', '0007_alter_response_choice'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medicalrecord',
            name='blood_type',
            field=models.CharField(blank=True, choices=[('A+', 'A+'), ('A-', 'A-'), ('B+', 'B+'), ('B-', 'B-'), ('AB+', 'AB+'), ('AB-', 'AB-'), ('O+', 'O+'), ('O-', 'O-')], default='', max_length=60),
        ),
    ]
