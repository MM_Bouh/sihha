from chat.serializers import ChatConversationSerializer
from django.db.models import query
from rest_framework import response
from rest_framework.views import APIView
from auth_api.models import CustomUser
from django.contrib.auth import get_user_model
from rest_framework import permissions
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView,
    DestroyAPIView,
    UpdateAPIView
)
# from chat.models import Chat, Contact
from chat.models import Conversation
from chat.views import get_user_contact
#from .serializers import ChatSerializer
from chat.serializers import ConversationSerializer

User = get_user_model()


class ChatListView(APIView):
    serializer_class = ChatConversationSerializer
    permission_classes = (permissions.AllowAny, )

    def get(self, request):
        #doctors_ids = Conversation.objects.filter(patient=pk)
        if request.user.role == 'PATIENT':
            conversations = request.user.patient.all()
            print("test")
            print(conversations)
            #queryset = request.user.patient.all()
        if request.user.role == 'DOCTOR':
            conversations = request.user.doctor.all()

        conversation_serializer = ChatConversationSerializer(
            conversations, many=True)
        print(conversation_serializer.data)
        return response.Response(conversation_serializer.data)
