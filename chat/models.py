from django.db import models
from auth_api.models import *
##########################################################################################


# Conversation Model
class Conversation(models.Model):

    # Choices for the conversation state
    ACTIVE = 'ACTIVE'
    PENDING = 'PENDING'
    COMPLETE = 'COMPLETE'
    STATES = [
        (ACTIVE, ACTIVE),
        (PENDING, PENDING),
        (COMPLETE, COMPLETE),
    ]

    # Model Attributes
    patient = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name='patient')
    doctor = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name='doctor')
    admin_agent = models.ForeignKey(
        CustomUser, on_delete=models.CASCADE, related_name='admin_agent')
    #messages = models.ManyToManyField(Message)
    state = models.CharField(
        max_length=50,
        choices=STATES,
        default=PENDING,
    )

    def __str__(self):
        return ' patient: '+self.patient.username + ' / doctor: '+self.doctor.username + ' / admin: '+self.admin_agent.username
        # return ' patient: '+self.patient.username+ ' / doctor: '+self.doctor.username + ' / admin: '+self.admin_agent.username + '/ id:'+str(self.id)


# Message Model
class Message(models.Model):

    # Model Attributes
    content = models.TextField(blank=True, null=True, max_length=1024)
    timestamp = models.DateTimeField(auto_now_add=True)
    conversation = models.ForeignKey(
        Conversation, on_delete=models.CASCADE, null=True)
    sender = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.content


##########################################################################################
