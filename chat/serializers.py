from rest_framework import serializers
from .models import *
from auth_api.models import CustomUser

##########################################################################################


class MessageSerializer(serializers.ModelSerializer):
    sender_img = serializers.ReadOnlyField(source='sender.profile_img_url')

    class Meta:
        model = Message
        fields = ('id', 'content', 'timestamp',
                  'conversation', 'sender', 'sender_img')

##########################################################################################


class ConversationSerializer(serializers.ModelSerializer):
    # participants ???
    class Meta:
        model = Conversation
        fields = '__all__'

##########################################################################################


class ChatConversationSerializer(serializers.ModelSerializer):
    messages = serializers.SerializerMethodField()

    def get_messages(self, obj):
        # messages = Message.objects.filter(conversation__id= obj.id)
        # serializer = MessageSerializer(messages, many=True)
        # return serializer.data

        messagesOb = Message.objects.filter(conversation__id=obj.id)
        messages = messagesOb.order_by('-timestamp').all()[:200]
        serializer = MessageSerializer(messages, many=True)
        return serializer.data

    # messages = Message.objects.filter(conversation=conversation.id)
    # serializer_messages = MessageSerializer(messages,many=True)
    doctor_name = serializers.ReadOnlyField(source='doctor.username')
    doctor_image = serializers.ReadOnlyField(source='doctor.profile_img_url')
    patient_name = serializers.ReadOnlyField(source='patient.username')
    patient_image = serializers.ReadOnlyField(source='patient.profile_img_url')
    patient_first_name = serializers.ReadOnlyField(source='patient.first_name')
    patient_last_name = serializers.ReadOnlyField(source='patient.last_name')
    doctor_first_name = serializers.ReadOnlyField(source='doctor.first_name')
    doctor_last_name = serializers.ReadOnlyField(source='doctor.last_name')

    class Meta:
        model = Conversation
        fields = ('id', 'state', 'patient', 'doctor',  'messages',
                  'doctor_name', 'doctor_image', 'patient_name',
                  'patient_image', 'patient_first_name', 'patient_last_name',
                  'doctor_first_name', 'doctor_last_name'
                  )
        #fields = '__all__'


##########################################################################################
class CreateConversationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conversation
        fields = ('id', 'patient', 'doctor', 'admin_agent',)

##########################################################################################


class CreateConversationSerializerByPatient(serializers.ModelSerializer):

    class Meta:
        model = Conversation
        fields = ('patient', 'doctor', 'admin_agent',)


##########################################################################################

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'last_name',
                  'first_name', 'role', 'profile_img_url')
