from rest_framework import response
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from .serializers import *
from .models import *
from auth_api.models import *
from auth_api.serializers import *
from django.shortcuts import get_object_or_404
from django.core import serializers


#################################################################################################################
# Get/Post Messages
@api_view(['POST'])
def message_view(request):

    # POST  Message
    if request.method == 'POST':
        serializer = MessageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return response.Response(serializer.data, status=status.HTTP_201_CREATED)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


##################################################################################################################
######## HERE I USED CLASS BASED VIEW TO GET THE BODY OF THE GET REQUEST (POSSIBLE ONLY WITH CLASS VIEW) #########
##################################################################################################################
# Get Patient/Doctor Conversations

class GetUserConversation(APIView):

    # get the Conversation as messages
    def get(self, request, format=None):
        patient = request.query_params['patient']
        doctor = request.query_params['doctor']
        conversation = Conversation.objects.get(patient=patient, doctor=doctor)
        messages = Message.objects.filter(conversation=conversation.id)
        serializer_messages = MessageSerializer(messages, many=True)
        print(messages)
        return response.Response(serializer_messages.data)

#################################################################################################################
# Get Doctors_list_conversations


@api_view(['GET'])
def get_doctors_list_conversations(request, pk):

    # 1.select the column of doctors in the conversations table where they share a Conversation with the patient
    doctors_ids = Conversation.objects.filter(patient=pk)
    doctors_ids = doctors_ids.values_list('doctor', flat=True)

    # 2.Get the doctors from their ids
    # Used the ''id__in'' to filter throught a list.
    doctors = CustomUser.objects.filter(id__in=doctors_ids)
    doctors_serializer = UserSerializer(doctors, many=True)

    return response.Response(doctors_serializer.data)


#################################################################################################################
# Get Patients_list_conversations
@api_view(['GET'])
def get_patients_list_conversations(request, pk):

    # 1.select the column of patients in the conversations table where they share a Conversation with the doctor
    patients_ids = Conversation.objects.filter(doctor=pk)
    patients_ids = patients_ids.values_list('patient', flat=True)

    # 2. Get the patients from their ids
    patients = CustomUser.objects.filter(id__in=patients_ids)
    patients_serializer = UserSerializer(patients, many=True)
    return response.Response(patients_serializer.data)


#################################################################################################################
# Get Doctor Conversations Number
@api_view(['GET'])
def get_count_conversations_doctor(request, pk):

    # 1.Get all the conversations where there is the doctor id
    conversations = Conversation.objects.filter(doctor=pk)
    total = conversations.count()
    active = conversations.filter(state='ACTIVE').count()
    pending = conversations.filter(state='PENDING').count()
    complete = conversations.filter(state='COMPLETE').count()
    return response.Response({'total': total, 'active': active, 'pending': pending, 'complete': complete})

#################################################################################################################
# Get Patient Conversations Number


@api_view(['GET'])
def get_count_conversations_patient(request, pk):

    # 1.Get all the conversations where there is the doctor id
    conversations = Conversation.objects.filter(patient=pk)
    total = conversations.count()
    active = conversations.filter(state='ACTIVE').count()
    pending = conversations.filter(state='PENDING').count()
    complete = conversations.filter(state='COMPLETE').count()
    return response.Response({'total': total, 'active': active, 'pending': pending, 'complete': complete})

#################################################################################################################
# Get Converstations administred by an admin_agent


@api_view(['GET'])
def get_admin_conversations(request, pk):

    # 1.get all converstions adminstred by the admin agent
    conversations = Conversation.objects.filter(admin_agent=pk)
    conversations_serializer = ConversationSerializer(conversations, many=True)

    # 2.get the info for the patients  (not just the ids)
    patients_ids = conversations.values_list('patient', flat=True)
    patients = CustomUser.objects.filter(id__in=patients_ids)
    patients_serializer = UserSerializer(patients, many=True)

    # 3. Create a for loop to check if the id of the user in conversations is equal to the serialized patients
    for i in range(len(patients_ids)):
        for j in range(len(patients_serializer.data)):
            if(patients_serializer.data[j]['id'] == conversations_serializer.data[i]['patient']):
                conversations_serializer.data[i]['patient'] = patients_serializer.data[j]['username']

    # 4.get the info for the doctors  (not just the ids)
    doctors_ids = conversations.values_list('doctor', flat=True)
    doctors = CustomUser.objects.filter(id__in=doctors_ids)
    doctors_serializer = UserSerializer(doctors, many=True)

    # 5. Create a for loop to check if the id of the user in conversations is equal to the serialized docotrs
    for i in range(len(doctors_ids)):
        for j in range(len(doctors_serializer.data)):
            if(doctors_serializer.data[j]['id'] == conversations_serializer.data[i]['doctor']):
                conversations_serializer.data[i]['doctor'] = doctors_serializer.data[j]['username']
    return response.Response(conversations_serializer.data)

#################################################################################################################
# Create a consultation by adminstratif agent


@api_view(['POST'])
def create_conversation(request):

    # 1.check if the conversation already exist
    conv_exist = Conversation.objects.filter(
        doctor=request.data['doctor'], patient=request.data['patient']).count()
    if(conv_exist > 0):
        return response.Response({'error': 'already exist'})
    else:
        # 2.create the conversation
        conversation = CreateConversationSerializer(data=request.data)

        # This is for creating an instance for the message sent by the doctor
        doctor = CustomUser.objects.get(id=request.data['doctor'])

        if(conversation.is_valid()):
            conv = conversation.save()
            # message_from_doctor = Message(
            #     content='Hello, How can I help you ?', conversation=conv, sender=doctor)
            # message_from_doctor.save()
            return response.Response(conversation.data, status=status.HTTP_201_CREATED)
        return response.Response(conversation.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def create_conversation_by_patient(request):

    admin = CustomUser.objects.get(tel=request.data['admin_agent'])

    # Check if the conv exists
    conv_exist = Conversation.objects.filter(
        doctor=request.data['doctor'], patient=request.data['patient']).count()
    if(conv_exist > 0):
        return response.Response({'error': 'already exist'})
    else:

        request.data['admin_agent'] = admin.id
        #request.data['patient'] = 200
        serializer = CreateConversationSerializerByPatient(
            #data={'patient': patient, 'doctor': doctor, 'admin_agent': admin}
            data=request.data
        )
        print(serializer)
        if serializer.is_valid():
            serializer.save()
            return response.Response(serializer.data, status=status.HTTP_201_CREATED)
        # print(serializer.errors)
        return response.Response({serializer.errors}, status=status.HTTP_400_BAD_REQUEST)


def get_last_10_messages(conversationId):
    print("This is the conversation id", conversationId)
    # conversation = get_object_or_404(Conversation, id=conversationId)
    conversation = Conversation.objects.get(id=conversationId)
    # print(conversation)
    serializers = ChatConversationSerializer(conversation)
    #print('This is the serialization result', serializers.data)
    #print('This is the serialization result for the messages', serializers.data['messages'])
    #messages = serializers.data['messages']
    # print(messages.order_by('-timestamp').all()[:10])
    return serializers.data['messages']


def get_user_contact(tel):
    # return get_object_or_404(CustomUser, username=username)
    return CustomUser.objects.get(tel=tel)


def get_current_chat(conversationId):
    return Conversation.objects.get(id=conversationId)
