from django.contrib.auth import get_user_model
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
from .models import Message
#Chat, Contact
from .views import get_last_10_messages, get_user_contact, get_current_chat, message_view

User = get_user_model()


class ChatConsumer(WebsocketConsumer):
    print("Hello, This is ChatConsumer Class")

    def fetch_messages(self, data):
        messages = get_last_10_messages(data['chatId'])
        content = {
            'command': 'messages',
            'messages': self.messages_to_json(messages)
        }

        self.send_message(content)

    def new_message(self, data):
        current_chat = get_current_chat(data['chatId'])
        sender = get_user_contact(data['from'])
        message = Message.objects.create(
            sender=sender,
            content=data['message'],
            conversation=current_chat
        )
        # current_chat = get_current_chat(data['chatId'])
        # print("This is the conversation: ", current_chat)
        # current_chat.messages.add(message)
        # It is added to the database but not the current chat

        # message.save()

        content = {
            'command': 'new_message',
            'message': self.new_message_to_json(message)
        }
        return self.send_chat_message(content)

    def messages_to_json(self, messages):
        result = []
        for message in messages:
            result.append(self.message_to_json(message))
        return result

    commands = {
        'fetch_messages': fetch_messages,
        'new_message': new_message
    }

    def new_message_to_json(self, message):
        return {
            'id': message.id,
            'author': message.sender.id,
            'content': message.content,
            'timestamp': str(message.timestamp),
            'sender_img': message.sender.profile_img_url
        }

    def message_to_json(self, message):
        ##################################### The right thing ###########################

        message = dict(message)
        return {
            'id': message['id'],
            'author': message['sender'],
            'content': message['content'],
            'timestamp': str(message['timestamp']),
            'sender_img': message['sender_img']

        }

    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive(self, text_data):
        data = json.loads(text_data)
        self.commands[data['command']](self, data)

    def send_chat_message(self, message):
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    def send_message(self, message):
        self.send(text_data=json.dumps(message))

    def chat_message(self, event):
        message = event['message']
        self.send(text_data=json.dumps(message))
