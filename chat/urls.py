from django.urls import path
from .views import *

urlpatterns = [

    path('messages/', message_view),
    path('patient_conversations_users/<int:pk>',
         get_doctors_list_conversations),
    path('doctor_conversations_users/<int:pk>',
         get_patients_list_conversations),
    path('user_conversations/', GetUserConversation.as_view()),
    path('count_conversations_doctor/<int:pk>', get_count_conversations_doctor),
    path('count_conversations_patient/<int:pk>',
         get_count_conversations_patient),
    # Admin_agent
    path('admin_agent_conversations/<int:pk>', get_admin_conversations),
    path('create_conversation/', create_conversation),
    path('create_conversation_by_patient/', create_conversation_by_patient)

]
